package fw.fwcore.Ui;


import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;

import java.util.Locale;

/**
 * com.fw.yahoo.auction.app.ui
 * YahooAuctionAndroid
 * <p/>
 * Created by alan.lam@fevaworks.net on 31/10/2014.
 * Copyright (c) 2014年 Alan Lam. All rights reserved.
 */
public class FWTextView extends TextView implements CustomFontHelper.CustomFontInterface  {

    private boolean mShouldCapitalize;
    private boolean mShouldLowercase;

    public FWTextView(Context context) {
        super(context);
        init(null);
    }

    public FWTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(attrs);
    }

    public FWTextView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        if(!isInEditMode()) {
            if(attrs != null) {
                CustomFontHelper.init(this, attrs);
            }
        }
    }

    /**
     *
     * @param fonts
     */
    public void setCustomText(String fonts){
        CustomFontHelper.setFont(this,fonts);
    }


    public void setShouldCapitalize(boolean mShouldCapitalize) {
        this.mShouldCapitalize = mShouldCapitalize;
    }

    public void setShouldLowercase(boolean mShouldLowercase) {
        this.mShouldLowercase = mShouldLowercase;
    }

    public void setText(String text) {
        if (text == null) {
            super.setText(null);
        } else if (mShouldCapitalize) {
            super.setText(text.toUpperCase(Locale.getDefault()));
        } else if (mShouldLowercase) {
            super.setText(text.toLowerCase(Locale.getDefault()));
        } else {
            super.setText(text);
        }
    }
}