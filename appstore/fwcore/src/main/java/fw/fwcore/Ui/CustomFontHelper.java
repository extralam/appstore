package fw.fwcore.Ui;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.support.v4.util.LruCache;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.style.MetricAffectingSpan;
import android.util.AttributeSet;
import android.widget.TextView;

import fw.fwcore.R;
import fw.fwcore.Utils.StringUtils;


/**
 * CustomFontHelper
 */
public class CustomFontHelper {

    private static LruCache<String, Typeface> fontCache = new LruCache<String, Typeface>(12);

    public static void init(CustomFontInterface cfi, AttributeSet attrs) {
        setFont(cfi, attrs);
        setCapitalize(cfi, attrs);
        setLowercase(cfi, attrs);
        setText(cfi, attrs);
        cfi.setIncludeFontPadding(false);
    }

    public static void init(CustomFontInterface cfi, String fontPath, boolean shouldCapitalize, boolean shouldLowercase) {
        setFont(cfi, fontPath);
        cfi.setShouldCapitalize(shouldCapitalize);
        cfi.setShouldLowercase(shouldLowercase);
        cfi.setIncludeFontPadding(false);
    }

    public static void setCustomFonts(TextView textView , String font){
        if (!StringUtils.isNullOrEmpty(font)) {
            Typeface typeface = fontCache.get(font);
            if (typeface == null) {
                try {
                    typeface = Typeface.createFromAsset(textView.getContext().getAssets(), font);
                    fontCache.put(font, typeface);
                    textView.setTypeface(typeface);
                }catch (RuntimeException e){
                }
            }else{
                textView.setTypeface(typeface);
            }
        }
    }

    public static void setFont(CustomFontInterface cfi, String font) {
        if (!StringUtils.isNullOrEmpty(font)) {
            _setFonts(cfi , font);
        } else {
            System.err.println("WARNING: No font specified for CustomFontInterface!");
        }
    }

    private static void _setFonts(CustomFontInterface cfi, String font){
        Typeface typeface = fontCache.get(font);
        if (typeface == null) {
            try {
                typeface = Typeface.createFromAsset(cfi.getContext().getAssets(), font);
                fontCache.put(font, typeface);
                cfi.setTypeface(typeface);
            }catch (RuntimeException e){
            }
        }else{
            cfi.setTypeface(typeface);
        }
    }

    public static Typeface getTypeface(Context context, String font) {
        Typeface typeface = fontCache.get(font);
        if (typeface == null) {
            typeface = Typeface.createFromAsset(context.getAssets(), font);
            fontCache.put(font, typeface);
        }
        return typeface;
    }

    private static void setFont(CustomFontInterface cfi, AttributeSet attrs) {
        final TypedArray a = cfi.getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        String font = a.getString(R.styleable.CustomFontTextView_font);
        setFont(cfi, font);
        a.recycle();
    }

    private static void setCapitalize(CustomFontInterface cfi, AttributeSet attrs) {
        final TypedArray a = cfi.getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        cfi.setShouldCapitalize(a.getBoolean(R.styleable.CustomFontTextView_capitalize, false));
        a.recycle();
    }

    private static void setLowercase(CustomFontInterface cfi, AttributeSet attrs) {
        final TypedArray a = cfi.getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        cfi.setShouldLowercase(a.getBoolean(R.styleable.CustomFontTextView_lowercase, false));
        a.recycle();
    }

    private static void setText(CustomFontInterface cfi, AttributeSet attrs) {
        final TypedArray a = cfi.getContext().obtainStyledAttributes(attrs, R.styleable.CustomFontTextView);
        CharSequence text = a.getText(R.styleable.CustomFontTextView_text);
        if (text != null)
            cfi.setText(text.toString());
        a.recycle();
    }

    public static SpannableString getTypeFaceSpan(Context ctx , String str,String fonts){
        SpannableString s = new SpannableString(str);
        s.setSpan(new TypefaceSpan(ctx, fonts), 0, s.length(),
                Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return s;
    }

    private static class TypefaceSpan  extends MetricAffectingSpan {
        private Typeface mTypeface;
        /**
         * Load the {@link Typeface} and apply to a {@link Spannable}.
         */
        public TypefaceSpan(Context context, String typefaceName) {
            mTypeface = CustomFontHelper.getTypeface(context , typefaceName);
        }

        @Override
        public void updateMeasureState(TextPaint p) {
            p.setTypeface(mTypeface);

            // Note: This flag is required for proper typeface rendering
            p.setFlags(p.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }

        @Override
        public void updateDrawState(TextPaint tp) {
            tp.setTypeface(mTypeface);

            // Note: This flag is required for proper typeface rendering
            tp.setFlags(tp.getFlags() | Paint.SUBPIXEL_TEXT_FLAG);
        }
    }

    public interface CustomFontInterface {
        public void setTypeface(Typeface tf);

        public void setIncludeFontPadding(boolean include);

        public void setShouldLowercase(boolean shouldLowercase);

        public void setShouldCapitalize(boolean shouldCapitalize);

        public Context getContext();

        public void setText(String text);

    }

}