package fw.fwcore.Ui;

import android.content.Context;
import android.util.AttributeSet;
import android.view.ViewGroup;
import android.widget.ListView;

/**
 * com.goxip.app.ui
 * Goxip
 * <p/>
 * Created by alan.lam@fevaworks.net on 28/11/2014.
 * Copyright (c) 2014年 Alan Lam. All rights reserved.
 */
public class FWWrappedListView extends ListView {

    public FWWrappedListView(Context context) {
        super(context);
    }

    public FWWrappedListView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FWWrappedListView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setOnScrollListener(OnScrollListener l) {
        super.setOnScrollListener(l);

    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // Calculate entire height by providing a very large height hint.
        // View.MEASURED_SIZE_MASK represents the largest height possible.
        int expandSpec = MeasureSpec.makeMeasureSpec(MEASURED_SIZE_MASK, MeasureSpec.AT_MOST);
        super.onMeasure(widthMeasureSpec, expandSpec);

        ViewGroup.LayoutParams params = getLayoutParams();
        params.height = getMeasuredHeight();


    }
}
