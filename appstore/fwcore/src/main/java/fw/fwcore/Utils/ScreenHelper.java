package fw.fwcore.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Rect;
import android.util.DisplayMetrics;
import android.view.Window;
import android.view.WindowManager;

/**
 * <p/>
 * Created by alan.lam
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 *
 *
 * Screen Helper
 */
public class ScreenHelper {

    private static Context sApplicationContext;
    private static int sNotificationBarHeight = -1;

    public static void setApplicationContext(Context context) {
        sApplicationContext = context;
    }

    private static DisplayMetrics getDisplaySize(WindowManager windowManager) {
        DisplayMetrics displaymetrics = new DisplayMetrics();
        windowManager.getDefaultDisplay().getMetrics(displaymetrics);
        return displaymetrics;
    }

    public static int getNotificationBarHeight(Activity activity) {
        if (sNotificationBarHeight == -1) {
            Rect rect = new Rect();
            Window window = activity.getWindow();
            window.getDecorView().getWindowVisibleDisplayFrame(rect);
            int height = rect.top;
            sNotificationBarHeight = height;
            return height;
        } else {
            return sNotificationBarHeight;
        }
    }

    public static int getScreenWidth() {
        WindowManager wm =
                (WindowManager) sApplicationContext.getSystemService(Context.WINDOW_SERVICE);
        return getDisplaySize(wm).widthPixels;
    }

    public static int getScreenHeight() {
        WindowManager wm =
                (WindowManager) sApplicationContext.getSystemService(Context.WINDOW_SERVICE);
        return getDisplaySize(wm).heightPixels;
    }

    public static int getActivityWidth(Activity activity) {
        return getDisplaySize(activity.getWindowManager()).widthPixels;
    }

    public static int getActivityProtraitWidth(Activity activity) {
        DisplayMetrics displayMetrics = getDisplaySize(activity.getWindowManager());
        return Math.min(displayMetrics.widthPixels, displayMetrics.heightPixels);
    }

    public static int getActivityHeight(Activity activity) {
        return getDisplaySize(activity.getWindowManager()).heightPixels - getNotificationBarHeight(activity);
    }

    public static boolean isScreenPortrait() {
        return sApplicationContext.getResources()
                .getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT;
    }
}