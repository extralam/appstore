package fw.fwcore.Utils;

import android.text.format.Time;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

/**
 * <p/>
 * Created by alan.lam
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 *
 * A Date Helper Class
 */
public class DateHelper {

    private static final Locale LOCALE_ENG = Locale.ENGLISH;
    private static final TimeZone sHONGKONG_TIMEZONE = TimeZone.getTimeZone("Etc/GMT-8");

    public interface DateFormat {
        String Y_M_D = "yyyy-MM-dd";
        String Y_M_D_H_M_S = "yyyy-MM-dd HH:mm:ss";//2014-06-30 23:59:59
        String H_M_S = " HH:mm:ss";
    }

    public static int getCurrentYear() {
        Time now = new Time();
        now.setToNow();
        return now.year;
    }

    public static long getTimeStamp(String dateString) {
        return getTimeStamp(dateString , DateFormat.Y_M_D);
    }

    public static long getTimeStamp(String dateString , String dateformat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateformat);
        formatter.setTimeZone(sHONGKONG_TIMEZONE);
        try {
            Date date = formatter.parse(dateString);
            return date.getTime();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }

    public static String getStringFromDate(Date date, String dateFormat) {
        return getStringFromDate(date, dateFormat, sHONGKONG_TIMEZONE);
    }

    public static String getStringFromDate(Date date, String dateFormat, TimeZone timeZone) {
        if (date == null) {
            return "";
        }
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, LOCALE_ENG);
        if (timeZone != null) {
            simpleDateFormat.setTimeZone(timeZone);
        }
        return simpleDateFormat.format(date);
    }

    public static boolean isInputDateLarger(String inputDateStr, String compareDateStr, String format) {
        SimpleDateFormat sdf = new SimpleDateFormat(format);
        Date targetDate = null;
        Date inputDate = null;
        try {
            targetDate = sdf.parse(compareDateStr);
            inputDate = sdf.parse(inputDateStr);
        } catch (Exception e) {
        }
        if (inputDate.after(targetDate)) {
            return true;
        }
        return false;
    }

}
