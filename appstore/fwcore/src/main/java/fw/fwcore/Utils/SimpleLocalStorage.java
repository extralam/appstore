package fw.fwcore.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import java.util.ArrayList;

/**
 *
 * SimpleLocalStorage
 *
 * <p/>
 * Created by alan on 18/4/14.
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 */
public class SimpleLocalStorage {

    public static String PREFS_NAME = "FevaworksProjectTemp";

    public static void putBoolean(Context ctx, String key, boolean data){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putBoolean(key,  data);
        editor.commit();
    }
    public static void putString(Context ctx, String key, String data){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(key,  data);
        editor.commit();
    }

    public static void putInt(Context ctx, String key, int data){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(key,  data);
        editor.commit();
    }

    public static void putLong(Context ctx, String key, long data){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putLong(key,  data);
        editor.commit();
    }

    public static int getInt(Context ctx, String key){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        return settings.getInt(key, 0);
    }

    public static long getLong(Context ctx, String key){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        return settings.getLong(key, 0);
    }

    public static String getString(Context ctx, String key){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        return settings.getString(key, null);
    }
    public static boolean getBoolean(Context ctx, String key){
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        return settings.getBoolean(key, false);
    }

    public static boolean saveArray(Context ctx , String name , ArrayList<String> data)
    {
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putInt(name + "_size" ,data.size());
        for(int i=0;i< data.size(); i++)
        {
            editor.remove(name + "_" + i);
            editor.putString(name + "_" + i, data.get(i));
        }
        return editor.commit();
    }
    public static ArrayList<String> loadArray(Context ctx, String name) {
        ArrayList<String> dataoutput = new ArrayList<String>();
        SharedPreferences settings = ctx.getSharedPreferences(PREFS_NAME, 0);
        int size = settings.getInt(name + "_size", 0);
        for(int i=0;i<size;i++)
        {
            dataoutput.add(settings.getString(name + "_" + i, null));
        }
        return dataoutput;
    }

    public static void clearAll(Context ctx){
        SharedPreferences preferences = ctx.getSharedPreferences(PREFS_NAME, 0);
        SharedPreferences.Editor editor = preferences.edit();
        editor.clear();
        editor.commit();
    }
}