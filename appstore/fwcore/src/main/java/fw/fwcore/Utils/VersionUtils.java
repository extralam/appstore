package fw.fwcore.Utils;


import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;


/**
 *
 * VersionUtils
 * StringUtils
 * <p/>
 * Created by alan on 27/7/14.
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 */
public class VersionUtils {

    public static boolean isV11OrHigher() {
        return Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB;
    }

    /**
     * Get Current SDK VERSION
     * @return
     */
    public static int getSDKVersion(){
        return Build.VERSION.SDK_INT;
    }

    /**
     * -1 if Package Not found
     * @param context
     * @return Get Applciation Package Version
     */
    public static int getPackageVersion(Context context){
        int version = -1;
        try {
            PackageInfo pInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), PackageManager.GET_META_DATA);
            version = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e1) {
        }
        return version;
    }

}
