package fw.fwcore.Utils;

import android.content.Context;
import android.database.Cursor;
import android.media.MediaScannerConnection;
import android.media.MediaScannerConnection.MediaScannerConnectionClient;
import android.net.Uri;
import android.provider.MediaStore;
import android.text.TextUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;

/**
 * <p/>
 * Created by alan.lam@fevaworks.net.
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 *
 *
 */
public class FileHelper {

    private static String getDirPath(Context context, String folderName) {
        String path = context.getCacheDir() + File.separator + folderName;
        makeFolder(path);
        return path;
    }

    public static boolean isSdcardAvailable() {
        return android.os.Environment.getExternalStorageState()
                .equals(android.os.Environment.MEDIA_MOUNTED);
    }

    public static String getSdcardPath() {
        return android.os.Environment.getExternalStorageDirectory().toString();
    }

    public static boolean isFolderExist(Context context, String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }
        File dir = new File(filePath);
        if(dir.exists() && dir.isDirectory()) {
            return true;
        }
        return false;
    }

    public static boolean isFileExist(String filePath) {
        if (TextUtils.isEmpty(filePath)) {
            return false;
        }
        return new File(filePath).exists();
    }

    public static void makeFolder(String folderPath) {
        File dir = new File(folderPath);
        dir.mkdirs();
    }

    public static void makeFile(String filePath) {
        File file = new File(filePath);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public static void clearFilesDir(Context context) {
        cleanDir(context.getFilesDir());
    }

    public static void clearExternalFilesDir(Context context){
        cleanDir(context.getExternalFilesDir(null));
    }

    public static void clearExternalCacheDir(Context context) {
        cleanDir(context.getExternalCacheDir());
    }

    private static void cleanDir(File dir) {
        File[] files = dir.listFiles();
        for (File file : files) {
            file.delete();
        }
    }

    public static void cleanDir(File dir, String containsPath) {
        File[] files = dir.listFiles();
        if (files == null) {
            return;
        }
        for (File file : files) {
            if (file.getPath().contains(containsPath)) {
                file.delete();
            }
        }
    }

    public static ArrayList<String> getFilesPath(File dir, String containsPath) {
        ArrayList<String > filePathList = new ArrayList<String>();
        File[] files = dir.listFiles();
        for (File file : files) {
            if (file.getPath().contains(containsPath)) {
                filePathList.add(file.getPath());
            }
        }
        return filePathList;
    }

    public static void removeFile(String path) {
        File file = new File(path);
        file.delete();
    }

    public static boolean writeToFile(byte[] data, String filePath) {
        boolean isSuccess = false;
        try {
            FileOutputStream fileOutputStream = new FileOutputStream(filePath);
            fileOutputStream.write(data);
            fileOutputStream.flush();
            fileOutputStream.close();
            isSuccess = true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return isSuccess;
    }

    public static String getRealPathFromURI(Context context, Uri contentUri) {
        String returnPath = "";
        Cursor cursor = null;
        try {
            String[] proj = {MediaStore.Images.Media.DATA};
            cursor = context.getContentResolver().query(contentUri, proj, null, null, null);
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();
            returnPath = cursor.getString(column_index);
        } catch (Exception e) {
            e.printStackTrace();
            returnPath = contentUri.getPath();
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return returnPath;
    }

    public static void copyFile(Context context, String originalPath, String destPath) {
        InputStream inputStream = null;
        OutputStream outputStream = null;
        try {
            inputStream = new FileInputStream(originalPath);
            outputStream = new FileOutputStream(destPath);
            copyFile(inputStream, outputStream);
            inputStream.close();
            inputStream = null;
            outputStream.flush();
            outputStream.close();
            outputStream = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void copyFile(InputStream in, OutputStream out) throws IOException {
        byte[] buffer = new byte[1024];
        int read;
        while((read = in.read(buffer)) != -1){
            out.write(buffer, 0, read);
        }
    }

    public static void scanFile(Context context, String path, String mimeType) {
        Client client = new Client(path, mimeType);
        MediaScannerConnection connection = new MediaScannerConnection(context, client);
        client.connection = connection;
        connection.connect();
    }

    private static final class Client implements MediaScannerConnectionClient {
        private final String path;
        private final String mimeType;
        MediaScannerConnection connection;

        public Client(String path, String mimeType) {
            this.path = path;
            this.mimeType = mimeType;
        }

        @Override
        public void onMediaScannerConnected() {
            connection.scanFile(path, mimeType);
        }

        @Override
        public void onScanCompleted(String path, Uri uri) {
            connection.disconnect();

        }
    }
}
