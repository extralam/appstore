package fw.fwcore.Utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

import java.math.BigInteger;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.nio.ByteOrder;

/**
 * <p/>
 * Created by alan.lam
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 *
 */
public class NetworkHelper {

    public static int TYPE_WIFI = 1;
    public static int TYPE_MOBILE = 2;
    public static int TYPE_NOT_CONNECTED = 0;

    private static Context sApplicationContext;

    public static void setApplicationContext(Context context) {
        sApplicationContext = context;
    }

    public static int getConnectivityStatus() {
        ConnectivityManager cm = (ConnectivityManager) sApplicationContext
                .getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        if (null != activeNetwork) {
            if(activeNetwork.getType() == ConnectivityManager.TYPE_WIFI)
                return TYPE_WIFI;
            if(activeNetwork.getType() == ConnectivityManager.TYPE_MOBILE)
                return TYPE_MOBILE;
        }
        return TYPE_NOT_CONNECTED;
    }

    public static String getConnectivityStatusString() {
        int conn = NetworkHelper.getConnectivityStatus();
        String status = null;
        if (conn == NetworkHelper.TYPE_WIFI) {
            status = "Wifi enabled";
        } else if (conn == NetworkHelper.TYPE_MOBILE) {
            status = "Mobile data enabled";
        } else if (conn == NetworkHelper.TYPE_NOT_CONNECTED) {
            status = "Not connected to Internet";
        }
        return status;
    }

    public static String getConnectivityStatusStringSimple() {
        int conn = NetworkHelper.getConnectivityStatus();
        String status = null;
        if (conn == NetworkHelper.TYPE_WIFI) {
            status = "Wifi";
        } else if (conn == NetworkHelper.TYPE_MOBILE) {
            status = "Mobile";
        } else if (conn == NetworkHelper.TYPE_NOT_CONNECTED) {
            status = "Not connected";
        }
        return status;
    }

    public static boolean isNetworkAvailable() {
        ConnectivityManager connectivity = (ConnectivityManager) sApplicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity != null) {
            NetworkInfo info = connectivity.getActiveNetworkInfo();
            if ((info != null) && (info.getState() == NetworkInfo.State.CONNECTED)) {
                return true;
            }
        }
        return false;
    }

    // http://stackoverflow.com/questions/16730711/get-my-wifi-ip-address-android
    public String getWiFiIpAddress() {
        final WifiManager mWifiManager = (WifiManager) sApplicationContext.getSystemService(Context.WIFI_SERVICE);
        int ipAddress = mWifiManager.getConnectionInfo().getIpAddress();

        // Convert little-endian to big-endianif needed
        if (ByteOrder.nativeOrder().equals(ByteOrder.LITTLE_ENDIAN)) {
            ipAddress = Integer.reverseBytes(ipAddress);
        }

        byte[] ipByteArray = BigInteger.valueOf(ipAddress).toByteArray();

        String ipAddressString;
        try {
            ipAddressString = InetAddress.getByAddress(ipByteArray).getHostAddress();
        } catch (UnknownHostException e) {
            Log.e("NETWORK", "Unable to get host address.");
            ipAddressString = null;
        }

        return ipAddressString;
    }
}
