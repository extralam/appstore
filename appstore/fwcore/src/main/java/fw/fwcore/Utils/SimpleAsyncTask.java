package fw.fwcore.Utils;

import android.annotation.TargetApi;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Build;

import java.lang.ref.WeakReference;

/**
 *
 * SimpleAsyncTask
 *
 * <p/>
 * Created by alan on 25/7/14.
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 */
abstract public class SimpleAsyncTask<T> extends
        AsyncTask<Object, Void, T> {

    private WeakReference<Activity> weakActivity;

    public SimpleAsyncTask(Activity activity) {
        weakActivity = new WeakReference<Activity>(activity);
    }

    abstract protected T onRun(Object... params);
    abstract protected void onSuccess(T result);

    @Override
    final protected T doInBackground(Object... params) {
        return onRun(params);
    }

    private boolean canContinue() {
        Activity activity = weakActivity.get();
        return activity != null && !activity.isFinishing() ;
    }

    @Override
    final protected void onPostExecute(T result) {
        if(weakActivity != null && canContinue()) {
            onSuccess(result);
        }else{
            onSuccess(result);
        }
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public SimpleAsyncTask execute() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
            return(this);
        }

        return (SimpleAsyncTask)(super.execute());
    }
}