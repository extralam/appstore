package fw.fwcore.Utils;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Picture;
import android.graphics.drawable.Drawable;
import android.view.View;
import android.webkit.WebView;

/**
 * <p/>
 * Created by alan.lam@fevaworks.net.
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 *
 */
public class SnapshotHelper {

    private static final int WEB_VIEW_SNAPSHOT_WIDTH = 900;

    public static Bitmap captureToBitmap(View view) {
        return captureToBitmap(view, null);
    }

    public static Bitmap captureToBitmap(View view, Drawable bgDrawable) {
        return captureToBitmap(view, bgDrawable, false);
    }

    public static Bitmap captureToBitmap(View view, Drawable bgDrawable, boolean isWebView) {
        Bitmap bitmap = null;
        if (!isWebView) {
            bitmap = Bitmap.createBitmap(view.getWidth(), view.getHeight()
                    , Bitmap.Config.RGB_565);
            Canvas canvas = new Canvas(bitmap);
            if (bgDrawable != null) {
                // draw the background first
                bgDrawable.draw(canvas);
            }
            view.draw(canvas);
        } else {
            Picture picture = ((WebView)view).capturePicture();
            float scaleFactor = 1.0f;
            if (picture.getWidth() < WEB_VIEW_SNAPSHOT_WIDTH) {
                scaleFactor = WEB_VIEW_SNAPSHOT_WIDTH / (float)picture.getWidth();
            }
            bitmap = Bitmap.createBitmap((int) (picture.getWidth() * scaleFactor)
                    , (int) (picture.getHeight() * scaleFactor)
                    , Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(bitmap);
            canvas.scale(scaleFactor, scaleFactor);
            picture.draw(canvas);
        }
        return bitmap;
    }


}
