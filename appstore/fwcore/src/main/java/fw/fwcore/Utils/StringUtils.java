package fw.fwcore.Utils;

import android.content.Context;
import android.telephony.TelephonyManager;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * com.fw.luvline.commonUtils
 * luvline
 * <p/>
 * Created by alan.lam@fevaworks.net on 19/8/2014.
 * Copyright (c) 2014年 Alan Lam. All rights reserved.
 */
public class StringUtils {

    private static int DEFAULT_ZOOM_LEVEL = 20;

    /**
     * Universal method to detect a null or empty string
     * Better than isEmpty() because it works on older Java system and apply trim (the original method doesn't)
     */
    public static boolean isNullOrEmpty(String string)
    {
        return string == null || string.trim().length() == 0;
    }

    public static String generateGoogleMapHtmlString(String address, double lat,double lnt){
        return generateGoogleMapHtmlString(address , DEFAULT_ZOOM_LEVEL , lat, lnt);
    }

    /**
     * Get Google Map URL
     * @param address   : address name
     * @param zoom      : zoom level
     * @param lat       : latitude
     * @param lnt       : longitude
     * @return          : google map url
     */
    private static String generateGoogleMapHtmlString(String address, int zoom , double lat,double lnt){
        if(isNullOrEmpty(address)){
            return "";
        }
        return "<a href=\"http://maps.google.com/maps?z=" + zoom +
                "&t=m&q=" + address +
                "&loc=" + lat + "," + lnt +
                "&ll=" + lat + "," + lnt + "\">" + address + "</a>";
    }

    /**
     * Get the Google PDF READER
     * @param pdf_path  : your pdf file in url
     * @return          : google pdf reader url
     */
    public static String getGooglePdfReaderPath(String pdf_path) {
        return "http://docs.google.com/gview?embedded=true&url=" + pdf_path;
    }

    public static String getFacebookProfilePic(String fbid , int SIZE){
        return "https://graph.facebook.com/" + fbid + "/picture?width=" + SIZE + "&height=" + SIZE;
    }

    /**
     * determine whether in the list
     * @param subset
     * @param superset
     * @return
     */
    public static boolean isSubsetOf(Collection<String> subset, Collection<String> superset) {
        for (String string : subset) {
            if (!superset.contains(string)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Check It is Valid Email or not
     * @param target
     * @return
     */
    public static boolean isValidEmail(String target) {
        if (isNullOrEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    /**
     * Get ISO 3166-1 alpha-2 country code for this device (or null if not available)
     * @param context Context reference to get the TelephonyManager instance from
     * @return country code or null
     */
    public static String getUserCountry(Context context) {
        try {
            final TelephonyManager tm = (TelephonyManager) context.getSystemService(Context.TELEPHONY_SERVICE);
            final String simCountry = tm.getSimCountryIso();
            if (simCountry != null && simCountry.length() == 2) { // SIM country code is available
                return simCountry.toLowerCase(Locale.US);
            }
            else if (tm.getPhoneType() != TelephonyManager.PHONE_TYPE_CDMA) { // device is not 3G (would be unreliable)
                String networkCountry = tm.getNetworkCountryIso();
                if (networkCountry != null && networkCountry.length() == 2) { // network country code is available
                    return networkCountry.toLowerCase(Locale.US);
                }
            }
        }
        catch (Exception e) {
            try {
                final String simCountry = context.getResources().getConfiguration().locale.getISO3Country();
                return simCountry.toLowerCase(Locale.US);
            }catch (Exception e2) {}
        }
        return null;
    }

    /**
     * Merges a string array into a single string with the specified separator
     */
    public static String mergeArray(String[] strings, String separator)
    {
        StringBuilder sb = new StringBuilder();
        for (String string : strings)
        {
            sb.append(string);
            sb.append(separator);
        }
        String result = sb.toString();
        result = result.substring(0, result.length() - 1);
        return result;
    }

    /**
     * Merges a string List into a single string with the specified separator
     */
    public static String mergeArray(List<String> strings, String separator)
    {
        StringBuilder sb = new StringBuilder();
        for (String string : strings)
        {
            sb.append(string);
            sb.append(separator);
        }
        String result = sb.toString();
        result = result.substring(0, result.length() - 1);
        return result;
    }

    public static Map<String, String> getQueryMap(String query) {
        if(query == null){
            return new HashMap<String, String>();
        }
        String[] params = query.split("&");
        Map<String, String> map = new HashMap<String, String>();
        for (String param : params) {
            String name = param.split("=")[0];
            String value = param.split("=")[1];
            map.put(name, value);
        }
        return map;
    }


}
