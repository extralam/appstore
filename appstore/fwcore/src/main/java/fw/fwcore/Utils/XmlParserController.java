package fw.fwcore.Utils;


import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.FileInputStream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

/**
 * com.fw.yahoo.auction.app.utils
 *
 * <p/>
 * XML Parser Controller
 * Created by alan lam in 2012
 * Copyright (c) 2012年 Alan Lam. All rights reserved.
 */
public class XmlParserController {

    //Parsers
    protected DocumentBuilderFactory dbf;
    protected DocumentBuilder db;
    protected Document doc;
    protected final String TAG = "XmlParserController";

    public XmlParserController(String filePath){
        try{
            dbf = DocumentBuilderFactory.newInstance();
            db = dbf.newDocumentBuilder();
            FileInputStream file = new FileInputStream(filePath);
            doc = db.parse(file);
            doc.getDocumentElement().normalize();

        }catch (Exception e) {
            simpleLog( 1, "XML Pasing Excpetion = " + e);
        }
    }

    protected String GetNodeValueByTag(Element e, String tag) {
        String retValue = "";
        NodeList nodeList = e.getElementsByTagName(tag);

        if(nodeList.getLength() > 0) {
            Node temp = nodeList.item(0);
            if(temp.hasChildNodes()) {
                Node temp2 = temp.getChildNodes().item(0);

                retValue = temp2.getNodeValue();
            }

            if(retValue != null) {
                return retValue;
            }
        }

        return "";
    }

    protected boolean GetNodeBooleanValueByTag(Element e, String tag) {
        String retValue = GetNodeValueByTag(e,tag);

        if(retValue != "")
        {
            retValue = retValue.replaceAll("(?i:yes)", "true");
            retValue = retValue.replaceAll("(?i:no)", "false");
            return Boolean.parseBoolean(retValue);
        }

        return true;
    }

    protected int GetNodeIntValueByTag(Element e, String tag) {
        String retValue = GetNodeValueByTag(e,tag);

        if(retValue != "")
            return Integer.parseInt(retValue);

        return 0;
    }

    protected float GetNodeFloatValueByTag(Element e, String tag) {
        String retValue = GetNodeValueByTag(e,tag);

        if(retValue != "")
            return Float.parseFloat(retValue);

        return 0.0f;
    }

    protected String GetNodeValueByTag(Document doc, String tag) {
        String retValue = "";
        NodeList nodeList =  doc.getElementsByTagName(tag);

        if(nodeList.getLength() > 0) {
            Node temp = nodeList.item(0);
            if(temp.hasChildNodes()) {
                Node temp2 = temp.getChildNodes().item(0);

                retValue = temp2.getNodeValue();
            }

            if(retValue != null) {
                return retValue;
            }
        }

        return retValue;
    }

    protected Element GetFirstElementByTag(Document doc, String tag) {
        Element retValue = null;

        NodeList nodeList = doc.getElementsByTagName(tag);
        if(nodeList.getLength() > 0) {
            retValue = (Element)nodeList.item(0);
        }

        return retValue;
    }

    protected Element GetFirstElementByTag(Element e, String tag) {
        Element retValue = null;

        NodeList nodeList = e.getElementsByTagName(tag);
        if (nodeList.getLength() > 0) {
            retValue = (Element)nodeList.item(0);
        }

        return retValue;
    }

    /**
     *
     * @param level - 1 debug, 2 error
     * @param log
     */
    protected void simpleLog(int level , String log){
        switch (level){
            case 1:
                Log.d(TAG, log);
                break;
            case 2:
                Log.e(TAG, log);
                break;

        }
    }
}
