package fw.fwcore.Utils;


import android.graphics.Color;
import android.text.SpannableString;
import android.text.TextPaint;
import android.text.method.LinkMovementMethod;
import android.text.method.MovementMethod;
import android.text.style.ClickableSpan;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by alan.lam
 *
 * Make TextView linkable in between the text
 */
public class TextViewHelper {

    public static void setLinkableTextView(TextView textView , String text , LinkSpanClass mLinkSpanClass){
        makeLinksFocusable(textView);
        textView.setText(makeLinkSpans(text, mLinkSpanClass));
    }

    private static class ClickableString extends ClickableSpan {
        private View.OnClickListener mListener;
        public ClickableString(View.OnClickListener listener) {
            mListener = listener;
        }
        @Override
        public void onClick(View v) {
            mListener.onClick(v);
        }
        @Override
        public void updateDrawState(TextPaint ds) {
            super.updateDrawState(ds);
            ds.setColor(Color.parseColor("#ff6801"));//set text color
            ds.setUnderlineText(true); // set to false to remove underline
        }
    }

    private static SpannableString makeLinkSpan(CharSequence text, View.OnClickListener listener) {
        SpannableString link = new SpannableString(text);
        link.setSpan(new ClickableString(listener), 0, text.length(),
                SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        return link;
    }

    private static SpannableString makeLinkSpans(String text , LinkSpanClass mLinkSpanClass) {
        SpannableString link = new SpannableString(text);
        for(int i = 0; i < mLinkSpanClass.mTextStrings.size(); i++){
            int start = text.lastIndexOf(mLinkSpanClass.mTextStrings.get(i));
            int length = mLinkSpanClass.mTextStrings.get(i).length();
            link.setSpan(new ClickableString(mLinkSpanClass.ls.get(i)), start, start + length,
                    SpannableString.SPAN_INCLUSIVE_EXCLUSIVE);
        }
        return link;
    }

    private static void makeLinksFocusable(TextView tv) {
        MovementMethod m = tv.getMovementMethod();
        if ((m == null) || !(m instanceof LinkMovementMethod)) {
            if (tv.getLinksClickable()) {
                tv.setMovementMethod(LinkMovementMethod.getInstance());
            }
        }
    }

    public static class LinkSpanClass{
        public ArrayList<String> mTextStrings;
        public ArrayList<View.OnClickListener> ls;
        public LinkSpanClass(){
            mTextStrings = new ArrayList<String>();
            ls = new ArrayList<View.OnClickListener>();
        }
        public void AddNewItems(String text ,View.OnClickListener l){
            mTextStrings.add(text);
            ls.add(l);
        }
    }
}