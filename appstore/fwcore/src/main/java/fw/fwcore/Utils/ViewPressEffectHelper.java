package fw.fwcore.Utils;

import android.os.Build;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.AlphaAnimation;

/**
 *
 * View Press Effect Helper
 * - usage : do some simple press effect like iOS
 *
 * Simple Usage:
 * ImageView img = (ImageView) findViewById(R.id.img);
 * ViewPressEffectHelper.attach(img)
 *
 * @author Lam @ HongKong
 *
 */
public class ViewPressEffectHelper {

    private static final String TAG = ViewPressEffectHelper.class.getName();

    /**
     * Attach the View which you want have a touch event
     * @param view - any view
     */
    public static void attach(View view){
        view.setOnTouchListener(new ASetOnTouchListener(view));
    }

    private static class ASetOnTouchListener implements View.OnTouchListener{

        final float FIXED_FULL = 1.0f;
        final float FIXED_HALF = 0.5f;
        final int FIXED_DURATION = 100;
        float alphaOrginally = 1.0f;

        public ASetOnTouchListener(View v){
            if (Build.VERSION.SDK_INT >= 11){
                alphaOrginally = v.getAlpha();
            }
        }
        @Override
        public boolean onTouch(View v, MotionEvent event) {
            switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                        final AlphaAnimation animation = new AlphaAnimation(FIXED_FULL, FIXED_HALF);
                        animation.setDuration(FIXED_DURATION);
                        animation.setFillAfter(true);
                        v.startAnimation(animation);
                    } else
                        v.animate().setDuration(FIXED_DURATION).alpha(FIXED_HALF);
                }
                break;

                case MotionEvent.ACTION_UP:
                case MotionEvent.ACTION_CANCEL:{
                    Log.e(TAG, "ACTION_UP || ACTION_CANCEL");
                    if (Build.VERSION.SDK_INT < Build.VERSION_CODES.HONEYCOMB) {
                        final AlphaAnimation animation = new AlphaAnimation(FIXED_HALF, FIXED_FULL);
                        animation.setDuration(FIXED_DURATION);
                        animation.setFillAfter(true);
                        v.startAnimation(animation);
                    } else
                        v.animate().setDuration(FIXED_DURATION).alpha(alphaOrginally);
                }
                break;
            }

            return false;
        }

    }
}