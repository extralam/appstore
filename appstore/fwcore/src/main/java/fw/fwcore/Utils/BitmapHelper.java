package fw.fwcore.Utils;

import android.content.ContentResolver;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.media.ExifInterface;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p/>
 * Created by alan.lam
 * Copyright (c) 2014�~ Alan Lam. All rights reserved.
 *
 * A Bitmap Helper
 */
public class BitmapHelper {

    private static Options sBitmapOptions;

    private static Options getBitmapOptions() {
        if (sBitmapOptions == null) {
            sBitmapOptions = new Options();
            sBitmapOptions.inPurgeable = true;
            sBitmapOptions.inInputShareable = true;
        }
        return sBitmapOptions;
    }

    public static Bitmap decodeFromRes(Context context, int resId){
        if(resId == 0) return null;
        InputStream inputStream = context.getResources().openRawResource(resId);
        Bitmap returnBitmap = BitmapFactory.decodeStream(inputStream, null, getBitmapOptions());
        try {
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnBitmap;
    }

    public static boolean saveBitmapToFile(Context context, Bitmap bitmap, String filePath) {
        return saveBitmapToFile(context, bitmap, filePath, 100);
    }

    public static boolean saveBitmapToFile(Context context, Bitmap bitmap,
                                           String filePath, int quality) {
        boolean isSuccess = false;
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            bitmap.compress(Bitmap.CompressFormat.PNG, quality, bytes);
            File outputFile = new File(filePath);
            outputFile.createNewFile();
            FileOutputStream fo = new FileOutputStream(outputFile);
            fo.write(bytes.toByteArray());
            fo.close();
            isSuccess = true;
        } catch (NullPointerException ex){
            Log.e("atest", "NullPointerException");
        } catch (Exception e) {
            e.printStackTrace();
        }

        return isSuccess;
    }

    public static int calculateInSampleSize(Options options, int reqWidth, int reqHeight) {
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }
        return inSampleSize;
    }

    public static Bitmap decodeFromFile(Context context, String filePath) {
        return decodeFromFile(context, filePath, 0, 0, 0);
    }

    public static Bitmap decodeFromFile(Context context, String filePath,
                                        int reqWidth, int reqHeight, int rotateAngle) {
        return decodeFromFile(context, filePath, reqWidth, reqHeight, rotateAngle, false);
    }

    public static Bitmap decodeFromFile(Context context, String filePath, int reqWidth,
                                        int reqHeight, int rotateAngle, boolean isReturnScaledBitmap) {
        if(TextUtils.isEmpty(filePath)) return null;
        Bitmap returnBitmap = null;
        try {
            final Options options = new Options();
            if (reqWidth != 0 && reqHeight != 0) {
                InputStream stream = new FileInputStream(filePath);
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(stream, null, options);
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                options.inJustDecodeBounds = false;
                stream.close();
            }
            InputStream inputStream = new FileInputStream(filePath);
            options.inInputShareable = true;
            options.inPurgeable = true;
            returnBitmap = BitmapFactory.decodeStream(inputStream, null, options);

            if (isReturnScaledBitmap) {
                int height = returnBitmap.getHeight();
                int width = returnBitmap.getWidth();

                if (height > reqHeight || width > reqWidth) {
                    double y = Math.sqrt((reqWidth * reqHeight) / (((double) width) / height));
                    double x = (y / height) * width;

                    Bitmap scaledBitmap =
                            Bitmap.createScaledBitmap(returnBitmap, (int) x, (int) y, true);
                    returnBitmap.recycle();
                    returnBitmap = scaledBitmap;
                }
            }

            if (rotateAngle != 0 ) {
                Matrix matrix = new Matrix();
                matrix.postRotate(rotateAngle);
                Bitmap rotatedBitmap = Bitmap.createBitmap(returnBitmap, 0, 0,
                        returnBitmap.getWidth(), returnBitmap.getHeight(), matrix, true);
                returnBitmap.recycle();
                returnBitmap = rotatedBitmap;
            }
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnBitmap;
    }
    public static Bitmap decodeFromUri(Context context, Uri uri) {
        return decodeFromUri(context, uri, 0, 0, 0, false);
    }
    public static Bitmap decodeFromUri(Context context, Uri uri, int reqWidth,
                                        int reqHeight, int rotateAngle, boolean isReturnScaledBitmap) {
        if(uri == null) return null;

        Bitmap returnBitmap = null;
        ContentResolver cr = context.getContentResolver();

        System.gc();
        try {
            final Options options = new Options();
            if (reqWidth != 0 && reqHeight != 0) {
                InputStream stream = cr.openInputStream(uri);
                options.inJustDecodeBounds = true;
                BitmapFactory.decodeStream(stream, null, options);
                options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);
                options.inJustDecodeBounds = false;
                stream.close();
            }
            InputStream inputStream = cr.openInputStream(uri);
            options.inInputShareable = true;
            options.inPurgeable = true;
            returnBitmap = BitmapFactory.decodeStream(inputStream, null, options);

            if (isReturnScaledBitmap) {
                int height = returnBitmap.getHeight();
                int width = returnBitmap.getWidth();

                if (height > reqHeight || width > reqWidth) {
                    double y = Math.sqrt((reqWidth * reqHeight) / (((double) width) / height));
                    double x = (y / height) * width;

                    Bitmap scaledBitmap =
                            Bitmap.createScaledBitmap(returnBitmap, (int) x, (int) y, true);
                    returnBitmap.recycle();
                    returnBitmap = scaledBitmap;
                }
            }

            if (rotateAngle != 0 ) {
                Matrix matrix = new Matrix();
                matrix.postRotate(rotateAngle);
                Bitmap rotatedBitmap = Bitmap.createBitmap(returnBitmap, 0, 0,
                        returnBitmap.getWidth(), returnBitmap.getHeight(), matrix, true);
                returnBitmap.recycle();
                returnBitmap = rotatedBitmap;
            }
            inputStream.close();

        } catch (IOException e) {
            e.printStackTrace();
        }
        return returnBitmap;
    }

    public static int getImageOrientation(String imagePath){
        int orientation = 0;
        try {
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_NORMAL);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return orientation;
    }

    public static int getAngleFromOrientation(int exifOrientation) {
        int angle = 0;
        switch (exifOrientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                angle = 0;
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                angle = 270;
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                angle = 180;
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                angle = 90;
                break;
        }
        return angle;
    }

    /** Creates and returns a new bitmap which is the same as the provided bitmap
     * but with horizontal or vertical padding (if necessary)
     * either side of the original bitmap
     * so that the resulting bitmap is a square.
     * @param bitmap is the bitmap to pad.
     * @return the padded bitmap.*/
    public static Bitmap padBitmap(Bitmap bitmap , int padding)
    {
        int paddingX = padding;
        int paddingY = padding;

        if (bitmap.getWidth() == bitmap.getHeight())
        {
//            paddingX = 0;
//            paddingY = 0;
        }
        else if (bitmap.getWidth() > bitmap.getHeight())
        {
            paddingX = 0;
            paddingY = bitmap.getWidth() - bitmap.getHeight();
        }
        else
        {
            paddingX = bitmap.getHeight() - bitmap.getWidth();
            paddingY = 0;
        }

        Bitmap paddedBitmap = Bitmap.createBitmap(
                bitmap.getWidth() + paddingX,
                bitmap.getHeight() + paddingY,
                Bitmap.Config.RGB_565);

        Canvas canvas = new Canvas(paddedBitmap);
        canvas.drawARGB(0xFF, 0xFF, 0xFF, 0xFF); // this represents white color
        canvas.drawBitmap(
                bitmap,
                paddingX / 2,
                paddingY / 2,
                new Paint(Paint.FILTER_BITMAP_FLAG));

        return paddedBitmap;
    }

}
