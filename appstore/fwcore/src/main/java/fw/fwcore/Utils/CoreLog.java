package fw.fwcore.Utils;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;


/**
 * <p/>
 * Created by alan.lam
 * Copyright (c) 2014年 Alan Lam. All rights reserved.
 *
 * A Simple App Log
 */
public class CoreLog {

    private static final String LOG_NAME = "#CoreLog#";

    /**
     * Update it to Project AppConfig
     * @return
     */
    public static boolean isDebug(){
        return true;
        //return AppConfig.isDebug();
    }

    public static void e(String msg) {
        if(CoreLog.isDebug()) {
            StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[3];
            String fileInfo = stackTrace.getFileName() + "(" + stackTrace.getLineNumber() + ") " + stackTrace.getMethodName();
            Log.e(LOG_NAME, fileInfo + ": " + msg);
        }
    }

    public static void d(String msg) {
        if(CoreLog.isDebug()) {
            StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[3];
            String fileInfo = stackTrace.getFileName() + "(" + stackTrace.getLineNumber() + ") " + stackTrace.getMethodName();
            Log.d(LOG_NAME, fileInfo + ": " + msg);
        }
    }

    public static void w(String msg) {
        if(CoreLog.isDebug()){
            StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[3];
            String fileInfo = stackTrace.getFileName() + "(" + stackTrace.getLineNumber() + ") " + stackTrace.getMethodName();
            Log.w(LOG_NAME, fileInfo + ": " + msg);
        }
    }

    public static void i(String msg) {
        if(CoreLog.isDebug()) {
            StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[3];
            String fileInfo = stackTrace.getFileName() + "(" + stackTrace.getLineNumber() + ") " + stackTrace.getMethodName();
            Log.i(LOG_NAME, fileInfo + ": " + msg);
        }
    }

    public static void v(String msg) {
        if(CoreLog.isDebug()) {
            StackTraceElement stackTrace = Thread.currentThread().getStackTrace()[3];
            String fileInfo = stackTrace.getFileName() + "(" + stackTrace.getLineNumber() + ") " + stackTrace.getMethodName();
            Log.v(LOG_NAME, fileInfo + ": " + msg);
        }
    }


    public static void toast(final Context context , final CharSequence message){
        if(CoreLog.isDebug()){
            if (context != null) {
                //noinspection OverlyBroadCatchBlock
                try {
                    Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
                } catch (Exception ignored) {
                    /* We don't care if this fails */
                }
            }
        }
    }

}
