package fw.fwcore;

import android.app.Application;

import fw.fwcore.Activity.BaseActivity;
import fw.fwcore.Api.WS;
import fw.fwcore.Utils.ScreenHelper;

/**
 * Created by alan on 21/11/2015.
 */
public class BaseApplication extends Application{

    // Check The Application isRunning
    protected static boolean sAppInForeground;

    @Override
    public void onCreate() {
        super.onCreate();
        BaseActivity.setAppOpenFromRootActivity(false);
        ScreenHelper.setApplicationContext(this);
        // this time we do not use Volley
        WS.init(this);
    }

    public static void setAppInForeground(boolean isInForeground) {
        sAppInForeground = isInForeground;
    }

    public static boolean isAppInForeground() {
        return sAppInForeground;
    }


}