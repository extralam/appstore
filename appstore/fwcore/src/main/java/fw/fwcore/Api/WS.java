package fw.fwcore.Api;

import android.content.Context;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.RequestHandle;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

/**
 *
 * WS - Web Services
 * SmartMobile
 * <p/>
 * Created by alan on 18/4/14.
 * Copyright (c) 2014年 Alan Lam. All rights reserved.
 */
public class WS {

    private static volatile WS sInstance;
    private AsyncHttpClient client;
    private Context mContext;

    private static final int MAX_RETRY_TIME = 0;
    private static final int MAX_TIMEOUT = 10000;

    public static final String API_RESP_SUCCESS = "isSuccess";
    public static final String API_RESP_ERROR_CODE = "errorCode";
    public static final String API_RESP_ERROR_MSG = "errorMsg";
    public static final String API_RESP_PAYLOAD = "payload";

    public static interface ErrorCodes {
        int COMMAND_NOT_FOUND = 100;
        int TOKEN_EXPIRED = 101;
    }

    public static void init(Context ctx) {
        if (sInstance == null)
            sInstance = new WS(ctx);
    }

    public WS(Context _mContext) {
        mContext = _mContext;
        initClient();
    }

    public void setApplicationContext(Context _mContext) {
        mContext = _mContext;
    }

    private void initClient() {
        client = new AsyncHttpClient();
        client.setMaxConnections(5);
        client.setUserAgent("Android_loopj/0");
        client.setTimeout(MAX_TIMEOUT);
        client.setMaxRetriesAndTimeout(MAX_RETRY_TIME, MAX_TIMEOUT);
    }

    public static WS get() {
        return sInstance;
    }


    public RequestHandle getdata(String url, TextHttpResponseHandler mJsonHttpResponseHandler) {
        return client.get(mContext, url, mJsonHttpResponseHandler);
    }

    public RequestHandle getdata(String url, RequestParams params, TextHttpResponseHandler mJsonHttpResponseHandler) {
        return client.get(mContext, url, params, mJsonHttpResponseHandler);
    }

    public RequestHandle postData(String url, TextHttpResponseHandler mJsonHttpResponseHandler) {
        return client.post(url, mJsonHttpResponseHandler);
    }

    public RequestHandle postData(String url, RequestParams params, TextHttpResponseHandler mJsonHttpResponseHandler) {
        return client.post(mContext, url, params, mJsonHttpResponseHandler);
    }

    public void removeAll(Context ctx, boolean mayInterruptIfRunning) {
        client.cancelRequests(ctx, mayInterruptIfRunning);
    }



}