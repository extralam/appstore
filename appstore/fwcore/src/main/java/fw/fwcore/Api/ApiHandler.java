package fw.fwcore.Api;

import android.os.AsyncTask;

import com.google.gson.Gson;
import com.loopj.android.http.TextHttpResponseHandler;

import org.apache.http.Header;

import java.lang.reflect.Type;

import fw.fwcore.Utils.CoreLog;

/**
 *
 * APIHandler
 *
 * <p/>
 * - Wrapped TextHttpResponseHandler
 * - We check the success or not in this class.
 *
 * @version 0.1
 *          <p/>
 *          Created by alan on 18/4/14.
 *          Copyright (c) 2014年 Alan Lam. All rights reserved.
 */
public class ApiHandler<T> extends TextHttpResponseHandler {

    private Type mmClazz;
    private ApiCallBack mmCallback;

    public ApiHandler(final Type clazz, ApiCallBack<T> cb){
        this.mmClazz = clazz;
        this.mmCallback = cb;
    }

    @Override
    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
        CoreLog.e(responseString);
        if (mmCallback != null) {
            mmCallback.onFail(responseString);
        }
    }

    @Override
    public void onSuccess(int statusCode, Header[] headers, String responseString) {
        CoreLog.d(responseString);
        new ParserManager<T>(mmClazz, responseString, mmCallback).execute();
    }

    public interface ApiCallBack<T> {
        void onSuccess(T results);
        void onFail(String reasons);
    }


    /**
     * A Background Job to bind Json to ClassObject {@link T}
     *
     * @param <T>
     */
    private class ParserManager<T> extends AsyncTask<Void, Void, T> {

        private Type mmClazz;
        private String mmData;
        private ApiCallBack<T> mmCallback;

        public ParserManager(Type clazz, String data, ApiCallBack<T> cb) {
            mmClazz = clazz;
            mmData = data;
            mmCallback = cb;
        }
        private T parse(String data) {
            try {
                return new Gson().fromJson(
                        data,
                        mmClazz);
            }catch (Exception e){
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected T doInBackground(Void... params) {
            return parse(mmData);
        }

        @Override
        protected void onPostExecute(T t) {
            CoreLog.i("onPostExecute");
            if (mmCallback != null && !isCancelled() && t != null) {
                CoreLog.i("onPostExecute:onSuccess(T)");
                mmCallback.onSuccess(t);
            }
        }
    }
}
