package fw.fwcore.Bus;

import com.squareup.otto.Bus;
import com.squareup.otto.ThreadEnforcer;

/**
 * Created by Feva16 on 24/11/2015.
 */
public final class BusProvider {

    private static final Bus sAPI_BUS = new Bus(ThreadEnforcer.ANY);
    private static final Bus sUI_BUS = new Bus();

    private BusProvider(){

    }

    public static Bus getAPIBusInstance(){
        return sAPI_BUS;
    }

    public static Bus getUIBusInstance(){
        return sUI_BUS;
    }

}
