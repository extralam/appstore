package fw.fwcore.Activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.ViewConfiguration;

import java.lang.reflect.Field;

import butterknife.ButterKnife;
import fw.fwcore.BaseApplication;
import fw.fwcore.Bus.BusProvider;


public abstract class BaseActivity extends AppCompatActivity {

    private static boolean sAppOpenFromRootActivity;
    private ProgressDialog mProgressDialog;
    private Toolbar mActionBarToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(withLayoutRes()) {
            setContentView(getLayoutResource());
            ButterKnife.bind(this);
            initDialog();
            if(withAPIBus()){
                BusProvider.getAPIBusInstance().register(this);
            }
            if(withUIBus()){
                BusProvider.getUIBusInstance().register(this);
            }
            if(withToolBar()) {
                mActionBarToolbar = getActionBarToolbar();
                ActionBar ab = getSupportActionBar();
                if (ab != null) {
                    ab.setDisplayHomeAsUpEnabled(isDisplayHomeAsUp());
                    ab.setHomeButtonEnabled(isDisplayHomeAsUp());
                    ab.setDisplayShowTitleEnabled(false);
                }

                if(!isDisplayHomeAsUp()){
                    getOverflowMenu();
                }
            }
            initView(savedInstanceState);
            init();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        BaseApplication.setAppInForeground(false);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BaseApplication.setAppInForeground(true);
    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(mProgressDialog != null){
            mProgressDialog.dismiss();
            mProgressDialog = null;
        }
        if(withAPIBus()){
            BusProvider.getAPIBusInstance().unregister(this);
        }
        if(withUIBus()){
            BusProvider.getUIBusInstance().unregister(this);
        }
    }

    protected abstract int getLayoutResource();
    protected abstract void initView(Bundle savedInstanceState);
    protected abstract void init();
    protected abstract int getToolBarId();

    /**
     * Get the ActionBar if have
     * @return mActionBarToolbar {@link android.support.v7.widget.Toolbar}
     */
    protected Toolbar getActionBarToolbar() {
        if (mActionBarToolbar == null) {
            mActionBarToolbar = (Toolbar) findViewById(getToolBarId());
            if (mActionBarToolbar == null) {
                throw new UnsupportedOperationException("Without Toolbar in this Activity");
            }
            setSupportActionBar(mActionBarToolbar);
        }
        return mActionBarToolbar;
    }

    public boolean isDisplayHomeAsUp(){
        return true;
    }

    /**
     * Check if the activity have layout resource
     * We may having transparent Activity for transition and calculation only
     * @return always true;
     */
    protected boolean withLayoutRes(){
        return true;
    }
    /**
     * Check if the activity have layout resource with toolbar {@link android.support.v7.widget.Toolbar}
     * @return always true;
     */
    protected boolean withToolBar(){
        return true;
    }

    protected boolean withUIBus(){
        return true;
    }

    protected boolean withAPIBus(){
        return true;
    }

    /**
     * Get Current Activity
     * @return Activity
     */
    protected BaseActivity getActivity(){
        return this;
    }

    protected String getClassName() {
        return ((Object) this).getClass().getName();
    }

    private void initDialog() {
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setCancelable(false);
        mProgressDialog.setMessage("Loading ... ");
    }

    public void showLoadingDialog(boolean isShow){
        if(mProgressDialog == null)
            return;
        if (isShow){
            mProgressDialog.show();
        }else{
            mProgressDialog.hide();
        }
    }

    public static void setAppOpenFromRootActivity(boolean isOpenFromRoot) {
        sAppOpenFromRootActivity = isOpenFromRoot;
    }

    public static boolean isAppOpenFromRootActivity() {
        return sAppOpenFromRootActivity;
    }

    /**
     * Restart the application
     */
    public void restartApplicaiton(Class TargetClazz){
        Intent intent = new Intent(this, TargetClazz);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        finish();
    }


    /**
     * Hack the physical Menu Button Problem
     * {@inheritDoc "http://stackoverflow.com/questions/9739498/android-action-bar-not-showing-overflow#comment19799431_13098824" }
     *
     */
    private void getOverflowMenu() {
        try {
            ViewConfiguration config = ViewConfiguration.get(this);
            Field menuKeyField = ViewConfiguration.class.getDeclaredField("sHasPermanentMenuKey");
            if(menuKeyField != null) {
                menuKeyField.setAccessible(true);
                menuKeyField.setBoolean(config, false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}