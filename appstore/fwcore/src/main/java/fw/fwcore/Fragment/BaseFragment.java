package fw.fwcore.Fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import fw.fwcore.Bus.BusProvider;

/**
 *
 * BaseFragment
 *
 * <p/>
 * Created by alan on 29/1/15.
 * Copyright (c) 2015年 Alan Lam. All rights reserved.
 */
public abstract class BaseFragment extends Fragment {

    protected boolean isDestroyed = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getLayout() , container , false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        initView(savedInstanceState);
        init();
        if(withAPIBus()){
            BusProvider.getAPIBusInstance().register(this);
        }
        if(withUIBus()){
            BusProvider.getUIBusInstance().register(this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        isDestroyed = true;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        ButterKnife.unbind(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        if(withAPIBus()){
            BusProvider.getAPIBusInstance().unregister(this);
        }
        if(withUIBus()){
            BusProvider.getUIBusInstance().unregister(this);
        }
    }

    public abstract int getLayout();
    public abstract void initView(Bundle savedInstanceState);
    public abstract void init();


    protected boolean withUIBus(){
        return true;
    }

    protected boolean withAPIBus(){
        return true;
    }


    protected View findViewById(int id){
        return getView().findViewById(id);
    }
    protected View findViewById(View v , int id){
        return v.findViewById(id);
    }

}