package com.ah_lam.appstore;


import com.ah_lam.appstore.utils.AppLog;
import com.ah_lam.appstore.utils.AppPref;

import fw.fwcore.BaseApplication;

/**
 * Created by Feva16 on 2/7/2015.
 */
public class LamApp extends BaseApplication {

    @Override
    public void onCreate() {
        super.onCreate();
        AppLog.i("## Applicaiton Start - onCreate ##");
        AppPref.setApplicationContext(this);
        AppLog.i("## Applicaiton Start - onCreate Finished ##");
    }


}
