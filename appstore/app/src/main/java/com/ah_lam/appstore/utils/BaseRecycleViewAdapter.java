package com.ah_lam.appstore.utils;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public abstract class BaseRecycleViewAdapter<T> extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public interface TYPE {
        int HEADER = 0;
        int ITEM = 1;
        int FOOTER = 2;
        int EMPTY = 666;
    }

    boolean isAdded = false;

    protected Context mmContext;
    protected ArrayList<T> mItems;

    private OnItemClickListener mItemClickListener;

    public BaseRecycleViewAdapter(Context context) {
        mmContext = context;
        mItems = new ArrayList<T>();
    }

    public BaseRecycleViewAdapter(Context context, ArrayList<T> items) {
        mmContext = context;
        mItems = items;
    }

    private OnItemClickListener mOnItemClickLitener;

    public void setOnItemClickLitener(OnItemClickListener mOnItemClickLitener) {
        this.mOnItemClickLitener = mOnItemClickLitener;
    }


    public T getItem(int position) {
        if (mItems.size() > 0 && (mItems.size() > position || position < 0)) {
            return mItems.get(position);
        } else {
            return null;
        }
    }

    /**
     * Appends the specified element to the end of the list.
     */
    public void add(T item) {
        mItems.add(item);
        notifyDataSetChanged();
    }

    /**
     * Inserts the specified element at the specified position in the list.
     */
    public void add(int position, T item) {
        mItems.add(position, item);
        notifyDataSetChanged();
    }

    /**
     * Appends all of the elements in the specified collection to the end of the
     * list, in the order that they are returned by the specified collection's
     * Iterator.
     */
    public void addAll(Collection<? extends T> items) {
        if (!isAdded) {
            isAdded = true;
            mItems.addAll(items);
            notifyDataSetChanged();
            isAdded = false;
        }
    }

    /**
     * Appends all of the elements to the end of the list, in the order that
     * they are specified.
     */
    public void addAll(T... items) {
        Collections.addAll(mItems, items);
        notifyDataSetChanged();
    }

    /**
     * Inserts all of the elements in the specified collection into the list,
     * starting at the specified position.
     */
    public void addAll(int position, Collection<? extends T> items) {
        if (!isAdded) {
            isAdded = true;
            mItems.addAll(position, items);
            notifyDataSetChanged();
            isAdded = false;
        }
    }

    /**
     * Inserts all of the elements into the list, starting at the specified
     * position.
     */
    public void addAll(int position, T... items) {
        for (int i = position; i < (items.length + position); i++) {
            mItems.add(i, items[i]);
        }
        notifyDataSetChanged();
    }

    /**
     * Removes all of the elements from the list.
     */
    public void clear() {
        mItems.clear();
        mItems = new ArrayList<T>();
        notifyDataSetChanged();
    }

    /**
     * Replaces the element at the specified position in this list with the
     * specified element.
     */
    public void set(int position, T item) {
        mItems.set(position, item);
        notifyDataSetChanged();
    }

    /**
     * Removes the specified element from the list
     */
    public void remove(T item) {
        mItems.remove(item);
        notifyDataSetChanged();
    }

    /**
     * Removes the element at the specified position in the list
     */
    public void remove(int position) {
        mItems.remove(position);
        notifyDataSetChanged();
    }

    /**
     * Removes all elements at the specified positions in the list
     */
    public void removePositions(Collection<Integer> positions) {
        ArrayList<Integer> positionsList = new ArrayList<Integer>(positions);
        Collections.sort(positionsList);
        Collections.reverse(positionsList);
        for (int position : positionsList) {
            mItems.remove(position);
        }
        notifyDataSetChanged();
    }

    /**
     * Removes all of the list's elements that are also contained in the
     * specified collection
     */
    public void removeAll(Collection<T> items) {
        mItems.removeAll(items);
        notifyDataSetChanged();
    }

    /**
     * Retains only the elements in the list that are contained in the specified
     * collection
     */
    public void retainAll(Collection<T> items) {
        mItems.retainAll(items);
        notifyDataSetChanged();
    }

    /**
     * Returns the position of the first occurrence of the specified element in
     * this list, or -1 if this list does not contain the element. More
     * formally, returns the lowest position <tt>i</tt> such that
     * <tt>(o==null&nbsp;?&nbsp;get(i)==null&nbsp;:&nbsp;o.equals(get(i)))</tt>,
     * or -1 if there is no such position.
     */
    public int indexOf(T item) {
        return mItems.indexOf(item);
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE.EMPTY){
            return getEmptyVH(viewGroup);
        }
        else if (viewType == TYPE.HEADER) {
            return getHeaderVH(viewGroup);
        } else if (viewType == TYPE.FOOTER) {
            return getFooterVH(viewGroup);
        } else
            return getItemVH(viewGroup, viewType);
    }

    @Override
    public void onBindViewHolder(final RecyclerView.ViewHolder viewHolder, final int position) {
        int viewType = getItemViewType(position);
        if (viewType == TYPE.EMPTY){
            onBindEmptyView(viewHolder);
        }
        else if (position == getHeaderPosition()){
            onBindHeaderView(viewHolder);
        }
        else if (position == getFooterPosition()){
            onBindFooterView(viewHolder);
        }
        else {
            int new_pos = position;
            if (getHeaderPosition() >= 0) {
                new_pos = position - 1;
            }
            if (getFooterPosition() >= 0) {
                new_pos = position - 1;
            }
            onBindItemView(viewHolder, new_pos );
        }
        if (mOnItemClickLitener != null) {
            viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mOnItemClickLitener.onItemClick(viewHolder.itemView, position);
                }
            });
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mItems.size()==0 && haveEmptyView()){
            return TYPE.EMPTY;
        }
        else if (position == getHeaderPosition()) {
            return TYPE.HEADER;
        }
        else if (position == getFooterPosition()){
            return TYPE.FOOTER;
        }
        else
            return TYPE.ITEM;
    }

    @Override
    public int getItemCount() {

        if ((mItems == null || mItems.size() ==0) && haveEmptyView()){
           return 1;
        }

        if (mItems == null || mItems.size() ==0) {
            return 0;
        }

        int total = mItems.size();

        if (getHeaderPosition() >= 0) {
            total += 1;
        }

        if (getFooterPosition() >= 0) {
            total += 1;
        }

        return total;
    }

    public ArrayList<T> getFulllList(){
        return mItems;
    }

    protected int getHeaderPosition(){
        return -1;
    }

    protected int getFooterPosition(){
        return -1;
    }

    protected RecyclerView.ViewHolder getHeaderVH(ViewGroup viewGroup){
        return null;
    }
    protected RecyclerView.ViewHolder getFooterVH(ViewGroup viewGroup){
        return null;
    }
    protected RecyclerView.ViewHolder getEmptyVH(ViewGroup viewGroup){
        return null;
    }
    protected abstract RecyclerView.ViewHolder getItemVH(ViewGroup viewGroup, int viewType);

    protected void onBindHeaderView(RecyclerView.ViewHolder viewHolder){}
    protected void onBindFooterView(RecyclerView.ViewHolder viewHolder){}
    protected void onBindEmptyView(RecyclerView.ViewHolder viewHolder){}
    protected abstract void onBindItemView(RecyclerView.ViewHolder viewHolder, int position);


    public String getString(int res) {
        return mmContext.getString(res);
    }

    public interface OnItemClickListener {
        void onItemClick(View view, int position);
    }

    public boolean isHeader(int position){
        return position == getHeaderPosition();
    }

    public boolean haveEmptyView(){ return false;}
}
