package com.ah_lam.appstore.activity;

import android.content.Context;
import android.widget.Filterable;

import com.ah_lam.appstore.utils.BaseRecycleViewAdapter;

import java.util.ArrayList;

/**
 * Created by alan on 27/8/2017.
 */

public abstract class BaseFilterRecycleViewAdapter<T> extends BaseRecycleViewAdapter<T> implements Filterable {

    public ArrayList<T> mFilteredList;

    public BaseFilterRecycleViewAdapter(Context context) {
        super(context);
    }

    @Override
    public T getItem(int position) {
        if (mFilteredList.size() > 0 && (mFilteredList.size() > position || position < 0)) {
            return mFilteredList.get(position);
        } else {
            return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (mFilteredList.size()==0 && haveEmptyView()){
            return TYPE.EMPTY;
        }
        else if (position == getHeaderPosition()) {
            return TYPE.HEADER;
        }
        else if (position == getFooterPosition()){
            return TYPE.FOOTER;
        }
        else
            return TYPE.ITEM;
    }

    @Override
    public int getItemCount() {
        if ((mFilteredList == null || mFilteredList.size() ==0) && haveEmptyView()){
            return 1;
        }
        if (mFilteredList == null || mFilteredList.size() ==0) {
            return 0;
        }
        int total = mFilteredList.size();
        if (getHeaderPosition() >= 0) {
            total += 1;
        }
        if (getFooterPosition() >= 0) {
            total += 1;
        }
        return total;
    }

}
