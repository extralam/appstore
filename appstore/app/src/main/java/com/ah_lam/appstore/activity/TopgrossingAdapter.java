package com.ah_lam.appstore.activity;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;

import com.ah_lam.appstore.DataManager.AppstoreDM;
import com.ah_lam.appstore.Models.Entry;
import com.ah_lam.appstore.Models.Feeds;
import com.ah_lam.appstore.R;
import com.ah_lam.appstore.utils.AppLog;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import fw.fwcore.Api.ApiHandler;
import fw.fwcore.Ui.FWTextView;


public class TopgrossingAdapter extends BaseFilterRecycleViewAdapter<Entry> {

    public TopgrossingAdapter(Context context) {
        super(context);
    }

    @Override
    protected RecyclerView.ViewHolder getItemVH(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.header_item, viewGroup, false);
        return new VHItem(v);
    }

    public void loadData(){
        AppstoreDM.recommand(new ApiHandler.ApiCallBack<Feeds>() {
            @Override
            public void onSuccess(Feeds results) {
                AppLog.d("result " + results);
                addAll(results.feed.entry);
            }

            @Override
            public void onFail(String reasons) {
                AppLog.e(reasons);
            }
        });
    }

    @Override
    protected void onBindItemView(RecyclerView.ViewHolder viewHolder, int position) {
        Entry mItem = getItem(position);
        VHItem vh = new VHItem(viewHolder.itemView);
        Glide.with(mmContext).load(mItem.images.get(1).label).into(vh.img);
        vh.title.setText(mItem.title.label);
        vh.category.setText(mItem.category.attributes.label);
    }

    class VHItem extends RecyclerView.ViewHolder{

        @Bind(R.id.img)
        ImageView img;
        @Bind(R.id.title)
        FWTextView title;
        @Bind(R.id.category)
        FWTextView category;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mItems;
                } else {
                    ArrayList<Entry> filteredList = new ArrayList<>();
                    for (Entry curr_item : mItems) {
                        if(curr_item.isContainWord(charSequence.toString())){
                            filteredList.add(curr_item);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Entry>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

}
