package com.ah_lam.appstore.utils;

import android.content.Context;

import fw.fwcore.Utils.SimpleLocalStorage;


/**
 * Created by Feva16 on 2/7/2015.
 */
public class AppPref {

    private static Context sApplicationContext;
    public static void setApplicationContext(Context ctx){
        sApplicationContext = ctx;
    }

    private final static String FIXED_ACCESSTOKEN = "FIXED_ACCESSTOKEN";

    public static void setAccessToken(String token){
        SimpleLocalStorage.putString(sApplicationContext, FIXED_ACCESSTOKEN, token);
    }
    public static String getAccessToken(){
        return SimpleLocalStorage.getString(sApplicationContext , FIXED_ACCESSTOKEN);
    }

}
