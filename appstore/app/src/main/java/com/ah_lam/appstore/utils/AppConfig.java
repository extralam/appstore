package com.ah_lam.appstore.utils;

/**
 * Created by Feva16 on 2/7/2015.
 */
public class AppConfig {

    private static final boolean D = true;
    private static final boolean USE_PROD_SERVER = false;

    public static final int FIXED_SPLASHING_TIME = 1000;

    public static boolean isDebug(){
        return D;
    }
    public static boolean isProuctionServer(){return USE_PROD_SERVER;}

}
