package com.ah_lam.appstore.activity;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.ImageView;

import com.ah_lam.appstore.Models.Entry;
import com.ah_lam.appstore.R;
import com.bumptech.glide.Glide;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import fw.fwcore.Ui.FWTextView;
import jp.wasabeef.glide.transformations.CropCircleTransformation;
import jp.wasabeef.glide.transformations.RoundedCornersTransformation;


public class AppstoreAdapter extends BaseFilterRecycleViewAdapter<Entry>  {

    TopgrossingAdapter mTopgrossingAdapter;
    LinearLayoutManager horizontalLinearLayoutManager;
    private int lastPosition = -1;

    public AppstoreAdapter(Context context) {
        super(context);
        mTopgrossingAdapter = new TopgrossingAdapter(context);
        mTopgrossingAdapter.loadData();
        horizontalLinearLayoutManager = new LinearLayoutManager(context);
        horizontalLinearLayoutManager.setOrientation(LinearLayoutManager.HORIZONTAL);
    }

    @Override
    protected int getHeaderPosition() {
        return 0;
    }

    @Override
    protected RecyclerView.ViewHolder getHeaderVH(ViewGroup viewGroup) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.header, viewGroup, false);
        return new VHHeaderItem(v);
    }

    @Override
    protected RecyclerView.ViewHolder getItemVH(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).
                inflate(R.layout.row_normal, viewGroup, false);
        return new VHItem(v);
    }

    @Override
    protected void onBindItemView(RecyclerView.ViewHolder viewHolder, int position) {
        Entry mItem = getItem(position);
        VHItem vh = new VHItem(viewHolder.itemView);
        if(position % 2 == 0){
            Glide.with(mmContext)
                    .load(mItem.images.get(1).label)
                    .bitmapTransform(
                            new RoundedCornersTransformation(mmContext, 16, 0, RoundedCornersTransformation.CornerType.ALL))
                    .into(vh.img);

        }else{
            Glide.with(mmContext)
                    .load(mItem.images.get(1).label)
                    .bitmapTransform(new CropCircleTransformation(mmContext))
                    .into(vh.img);
        }
        vh.number.setText("" + (position + 1));
        vh.title.setText(mItem.title.label);
        vh.category.setText(mItem.category.attributes.label);
    }

    class VHItem extends RecyclerView.ViewHolder{

        @Bind(R.id.number)
        FWTextView number;
        @Bind(R.id.img)
        ImageView img;
        @Bind(R.id.title)
        FWTextView title;
        @Bind(R.id.category)
        FWTextView category;

        public VHItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    @Override
    protected void onBindHeaderView(RecyclerView.ViewHolder viewHolder) {
        VHHeaderItem vh = new VHHeaderItem(viewHolder.itemView);
        vh.recyclerView.setLayoutManager(horizontalLinearLayoutManager);
        vh.recyclerView.setAdapter(mTopgrossingAdapter);

    }

    class VHHeaderItem extends RecyclerView.ViewHolder{

        @Bind(R.id.header_title)
        FWTextView header_title;
        @Bind(R.id.recyclerView)
        RecyclerView recyclerView;

        public VHHeaderItem(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }

    public void performFilter(CharSequence newText){
        getFilter().filter(newText);
        if(mTopgrossingAdapter != null){
            mTopgrossingAdapter.getFilter().filter(newText);
        }
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    mFilteredList = mItems;
                } else {
                    ArrayList<Entry> filteredList = new ArrayList<>();
                    for (Entry curr_item : mItems) {
                        if(curr_item.isContainWord(charSequence.toString())){
                            filteredList.add(curr_item);
                        }
                    }
                    mFilteredList = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = mFilteredList;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                mFilteredList = (ArrayList<Entry>) filterResults.values;
                notifyDataSetChanged();
            }
        };
    }
}
