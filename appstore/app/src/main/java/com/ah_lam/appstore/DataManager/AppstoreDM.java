package com.ah_lam.appstore.DataManager;

import com.ah_lam.appstore.Models.Feeds;
import com.loopj.android.http.RequestParams;

import fw.fwcore.Api.ApiHandler;

public class AppstoreDM extends BaseDM{

    public static void list(ApiHandler.ApiCallBack<Feeds> mCallBack){
        RequestParams params = new RequestParams();
        wsGet(getApiUrlWithoutTimeStamp("rss/topfreeapplications/limit=100/json") ,
                params ,
                new ApiHandler(Feeds.class , mCallBack));
    }

    public static void recommand(ApiHandler.ApiCallBack<Feeds> mCallBack){
        RequestParams params = new RequestParams();
        wsGet(getApiUrlWithoutTimeStamp("rss/topgrossingapplications/limit=10/json") ,
                params ,
                new ApiHandler(Feeds.class , mCallBack));
    }



}
