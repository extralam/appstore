package com.ah_lam.appstore.activity;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import com.ah_lam.appstore.DataManager.AppstoreDM;
import com.ah_lam.appstore.Models.Feeds;
import com.ah_lam.appstore.R;
import com.ah_lam.appstore.utils.AppLog;

import butterknife.Bind;
import fw.fwcore.Activity.BaseActivity;
import fw.fwcore.Api.ApiHandler;

public class MainActivity extends BaseActivity implements SwipeRefreshLayout.OnRefreshListener{

    @Bind(R.id.swipeRefreshLayout)
    public SwipeRefreshLayout mSwipeRefreshLayout;
    @Bind(R.id.recyclerView)
    public RecyclerView mRecyclerView;
    public SearchView searchView;

    public LinearLayoutManager verticalLinearLayoutManager;
    public AppstoreAdapter mAppstoreAdapter;

    @Override
    protected int getLayoutResource() {
        return R.layout.activity_main;
    }

    @Override
    protected void initView(Bundle savedInstanceState) {

    }

    @Override
    protected int getToolBarId() {
        return R.id.toolbar;
    }

    @Override
    protected void init() {
        AppLog.d("init");
        getActionBarToolbar().setTitle(R.string.app_name);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }

        mAppstoreAdapter = new AppstoreAdapter(this);
        verticalLinearLayoutManager = new LinearLayoutManager(getActivity());
        verticalLinearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        mRecyclerView.setLayoutManager(verticalLinearLayoutManager);
        mRecyclerView.setAdapter(mAppstoreAdapter);
        mSwipeRefreshLayout.setOnRefreshListener(this);

        onRefresh();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem search = menu.findItem(R.id.search);
        searchView = (SearchView) MenuItemCompat.getActionView(search);
        search(searchView);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        return super.onOptionsItemSelected(item);
    }

    private void search(SearchView searchView) {

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (mAppstoreAdapter != null) {
                    mAppstoreAdapter.performFilter(newText);
                }
                return true;
            }
        });
    }

    @Override
    public void onRefresh() {
        AppstoreDM.list(new ApiHandler.ApiCallBack<Feeds>() {
            @Override
            public void onSuccess(Feeds results) {
                AppLog.d("result " + results);
                mSwipeRefreshLayout.setRefreshing(false);
                mAppstoreAdapter.clear();
                mAppstoreAdapter.addAll(results.feed.entry);
                if(searchView != null){
                    mAppstoreAdapter.performFilter(searchView.getQuery());
                }else {
                    mAppstoreAdapter.performFilter("");
                }
            }

            @Override
            public void onFail(String reasons) {
                mSwipeRefreshLayout.setRefreshing(false);
                AppLog.e(reasons);
            }
        });
    }

}
