package com.ah_lam.appstore.Models;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by alan on 27/8/2017.
 */

public class Entry {

    public Label title;

    @SerializedName(value="im:image")
    public ArrayList<FeedImage> images;

    public Category category;
    public Summary summary;

    public boolean isContainWord(String str){
        boolean isContainWord = false;
        isContainWord |= title.label.toLowerCase().contains(str);
        isContainWord |= summary.label.toLowerCase().contains(str);
        isContainWord |= category.attributes.label.toLowerCase().contains(str);
        return isContainWord;
    }
}
