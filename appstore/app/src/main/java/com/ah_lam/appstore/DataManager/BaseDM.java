package com.ah_lam.appstore.DataManager;

import android.util.Log;

import com.ah_lam.appstore.utils.AppConfig;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.TextHttpResponseHandler;

import fw.fwcore.Api.WS;

/**
 *
 * Please Check Loopo WebServices
 * http://loopj.com/android-async-http/
 *
 */
public class BaseDM {

    private static final boolean showUrl = true;
    public static final String DUMMY_URL = "http://xxxx.com";

    private static final String DEVELOPMENT_BASE_URL = "https://itunes.apple.com/hk/";
    private static final String PRODUCTION_BASE_URL = "https://itunes.apple.com/hk/";


    protected static String getBaseUrl() {
        if (AppConfig.isProuctionServer()) {
            return PRODUCTION_BASE_URL;
        } else {
            return DEVELOPMENT_BASE_URL;
        }
    }

    /**
     * @param cmd the Command of the API
     * @return url + api?cmd= + cmd  //+ #timestamp
     */
    protected static String getApiUrl(String cmd) {
        return new StringBuilder().append(getBaseUrl())
//                .append(APPEND_BASE_URL)
                .append(cmd)
                .append("#")
                .append(System.currentTimeMillis())
                .toString();
    }

    /**
     * @param cmd the Command of the API
     * @return url + api?cmd= + cmd
     */
    protected static String getApiUrlWithoutTimeStamp(String cmd) {
        return new StringBuilder().append(getBaseUrl())
//                .append(APPEND_BASE_URL)
                .append(cmd)
                .toString();
    }



    /**
     * Simple Fast System print out for the url
     * @param url                           An absolute URL giving the base location of the api
     */
    private static void u(String url ){
        if(showUrl)
            Log.d("#BASEDM#", url);
    }

    /**
     * Simple Post Method Handler , no Request Params
     * @param url                           An absolute URL giving the base location of the api
     * @param handler      The Http Response Handler from the httpclient
     */
    protected static void wsPost(String url , TextHttpResponseHandler handler){
        u(url);
        WS.get().postData(url, handler);
    }
    /**
     * Simple Post Method Handler
     * @param url                           An absolute URL giving the base location of the api
     * @param params                        The location of the api, relative to the url argument
     * @param handler      The Http Response Handler from the httpclient
     */
    protected static void wsPost(String url , RequestParams params , TextHttpResponseHandler handler){
        u(url);
        WS.get().postData(url, params, handler);
    }

    /**
     * Simple Get Method Handler, no Request Params
     * @param url                           An absolute URL giving the base location of the api
     * @param handler      The Http Response Handler from the httpclient
     */
    protected static void wsGet(String url , TextHttpResponseHandler handler){
        u(url);
        WS.get().getdata(url, handler);
    }


    /**
     * Simple Get Method Handler
     * @param url                           An absolute URL giving the base location of the api
     * @param params                        The location of the api, relative to the url argument
     * @param handler      The Http Response Handler from the httpclient
     */
    protected static void wsGet(String url , RequestParams params , TextHttpResponseHandler handler){
        u(url);
        WS.get().getdata(url, params, handler);
    }


}
